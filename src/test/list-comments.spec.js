const cucumber = require('cucumber');
const assert = require('chai').assert;


const makeListComments = require('../use-cases/list-comments')


let commentsDb={
    findByPostId(postId,omitReplies){
        return [{
            id:"n2j13mascnasAaaaccasdwcxa5",
            author:"Me",
            createdOn:1589439055474,
            hash:"4ce47b027d49e5833431c1b35397fd69",
            modifiedOn:1589439055474,
            postId:"cjt65art5350vy000hm1rp3s9",
            published:true,
            source:{
                ip:"::1",
                browser:"PostmanRuntime/7.6"
            }
        }]
    }
}
let postId="",result="",omitReplies=false;

const listComments = makeListComments({commentsDb})
cucumber.Given("postId is provided",function(){
    postId = "cjt65art5350vy000hm1rp3s9"
})

cucumber.When("comments are searched for the given postId",async function(){
     result = await listComments({postId,omitReplies}) 
     console.log(result)
})

cucumber.Then('return all comments', function () {
    assert.equal(result.length,1);
    assert.equal(result[0].postId,"cjt65art5350vy000hm1rp3s9")
    return 'success';
});
