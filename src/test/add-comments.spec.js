const cucumber = require('cucumber')
const assert = require('chai').assert
const sinon = require('sinon')
const makeAddComment = require('../use-cases/add-comment')
const makeHandleModeration = require('../use-cases/handle-moderation')

let commentsDb={},commentInfo={};
let result='';

commentsDb.findByHash = function findByHash(){}
commentsDb.insert = function insert(){}
sinon.stub(commentsDb,'findByHash').returns(false)
sinon.stub(commentsDb,'insert').returns({
    author: "XYZ",
    createdOn: Date.now(),
    id: "asa123ndlasdn1u2od",
    modifiedOn: Date.now(),
    postId: "cjt65art5350vy000hm1rp3s9",
    published: true,
    replyToId: "",
    text: "Awesome!!",
    source: {
      ip: "::1",
      browser: "GoogleChrome",
    }
})

const handleModeration= makeHandleModeration({isQuestionable:()=>false, initiateReview:()=>{}})

commentInfo={
    author:"", text:"",postId:"",
    source:{
        ip:"", browser:""
    }
}

cucumber.Given('comment has required details',()=>{
    commentInfo.author="XYZ"
    commentInfo.text="Awesome!!"
    commentInfo.postId="cjt65art5350vy000hm1rp3s9"
    commentInfo.source.ip="::1"
    commentInfo.browser="GoogleChrome"
    return 'success'
})

cucumber.When('comment is made', async ()=>{
const addComment = makeAddComment({commentsDb,handleModeration});
result = await addComment(commentInfo)

})

cucumber.Then('return a posted object', function () {

    assert.equal(Object.keys(result).length,9);
    assert.equal(result.author,"XYZ");
    assert.equal(result.postId,"cjt65art5350vy000hm1rp3s9");
    assert.equal(result.text,"Awesome!!")
  });
