// import cuid from 'cuid'
const  cuid = require('cuid')

const Id = Object.freeze({
  makeId: cuid,
  isValidId: cuid.isCuid
})

module.exports = Id
